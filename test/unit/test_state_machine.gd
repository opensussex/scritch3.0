extends "res://addons/gut/test.gd"

var ScanCodes = load("res://classes/ScanCodes.gd")
var KeyboardMocker = load("res://test/mocks/KeyboardMocker.gd").new()
var StateMachine = load("res://classes/StateMachine.gd")
var StatesObj = load("res://classes/States.gd").new()
var State = load("res://classes/State.gd")

var states = StatesObj.states

func test_initiate_statemachine():
	var sm = StateMachine.new(states)
	assert_eq(sm.state.name, states[0].name)

func test_cycle_state_one_place():
	var sm = StateMachine.new(states)
	sm.cycle_state()
	assert_eq(sm.state.name, states[1].name)

func test_cycle_state_two_places():
	var sm = StateMachine.new(states)
	sm.cycle_state()
	sm.cycle_state()
	assert_eq(sm.state.name, states[2].name)

func test_cycle_state_three_places_back_to_start():
	var sm = StateMachine.new(states)
	sm.cycle_state()
	sm.cycle_state()
	sm.cycle_state()
	#we go back round to the start
	assert_eq(sm.state.name, states[0].name)
	
func test_cycle_state_four_places_back_to_start():
	var sm = StateMachine.new(states)
	sm.cycle_state()
	sm.cycle_state()
	sm.cycle_state()
	sm.cycle_state()
	#we go back round to the start
	assert_eq(sm.state.name, states[1].name)
	
func test_cycle_state_with_key_combination_control_d():
	var sc = ScanCodes.new()
	var sm = StateMachine.new(states)
	var scancode = sc.get_input_readable_scancode_with_modifer(KeyboardMocker.make_keypress_with_modifier(KEY_D, "control"))
	if (scancode == 'Control+D'):
		sm.cycle_state()
		
	assert_eq(sm.state.name, states[1].name)

func test_set_state_by_name():
	var sm = StateMachine.new(states)
	var state_name = "edit"
	sm.set_state_by_name(state_name)
	assert_eq(sm.state.name, "edit")
	assert_eq(sm.state_index, 2)
