extends "res://addons/gut/test.gd"

var ScanCodes = load("res://classes/ScanCodes.gd")
var KeyboardMocker = load("res://test/mocks/KeyboardMocker.gd").new()
var HotKeys = load("res://classes/HotKeys.gd").new({
	"Control+A" : "_return_two"
})

var scancodes_for_testing = HotKeys.keys

func test_set_scancodes():
	var sc = ScanCodes.new()
	sc.set_codes(scancodes_for_testing)
	assert_eq(sc.has("Control+A"), true)
	
func _return_two():
	return 2
	
func test_get_method_and_run_from_scancode():
	var sc = ScanCodes.new()
	sc.set_codes(scancodes_for_testing)
	var method = sc.get_method("Control+A")
	var method_called_value = call(method)
	assert_eq(method_called_value, 2)
	

func test_scancode_key_a():
	var sc = ScanCodes.new()
	var scancode = sc.get_input_scancode(KeyboardMocker.make_keypress(KEY_A))
	assert_eq(scancode, 65)
	
func test_readable_scancode_key_a():
	var sc = ScanCodes.new()
	var scancode = sc.get_input_readable_scancode(KeyboardMocker.make_keypress(KEY_A))
	assert_eq(scancode, 'A')

func test_readable_scancode_key_alt_and_a():
	var sc = ScanCodes.new()
	var scancode = sc.get_input_readable_scancode_with_modifer(KeyboardMocker.make_keypress_with_modifier(KEY_A, "alt"))
	assert_eq(scancode, 'Alt+A')	
	
func test_readable_scancode_key_control_and_a():
	var sc = ScanCodes.new()
	var scancode = sc.get_input_readable_scancode_with_modifer(KeyboardMocker.make_keypress_with_modifier(KEY_A, "control"))
	assert_eq(scancode, 'Control+A')	
	
