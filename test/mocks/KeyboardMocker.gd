class_name KeyboardMocker

func make_keypress(key):
	var event = InputEventKey.new()
	event.set_scancode(key)
	event.set_pressed(true)
	return event
	
func make_keypress_with_modifier(key, modifer):
	
	var event = make_keypress(key)
	match modifer:
		"alt":
			event.set_alt(true)
		"comand":
			event.set_command(true)
		"control":
			event.set_control(true)
		"meta":
			event.set_meta(true)
		"shift":
			event.set_shift(true)
	return event
