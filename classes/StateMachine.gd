class_name StateMachine

var starting_state = null
var state = starting_state
var state_index = null
var states = []

func _init(new_states):
	states = new_states
	state = states[0]
	state_index = 0
	
func set_state_to_start():
	state = starting_state
	state_index = 0
	return state
	
func set_state_by_name(state_name):
	for i in range(0, states.size()):
		if(states[i].name == state_name):
			state = states[i]
			state_index = i

func cycle_state():
	state_index = state_index + 1
	if (_is_state_index_out_of_bounds()):
		state_index = 0;
	state = states[state_index]

func _is_state_index_out_of_bounds():
	if(state_index > states.size()-1) :
		return true
	return false
