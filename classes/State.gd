class_name State

var name = null
var description = null
var command = null
var focus = null

func _init(state = {}):
	if (!state.empty()):
		if(state.has("name")):
			name = state.name
		if(state.has("description")):
			description = state.description
		if(state.has("command")):
			command = state.command
		if(state.has("focus")):
			focus = state.focus
