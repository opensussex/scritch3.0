class_name ScanCodes

var codes = {}

func _init():
	pass

func get_codes():
	return codes

func set_codes(new_codes):
	codes = new_codes
	
func has(key):
	return codes.has(key)

func get_method(scancode):
	return codes[scancode]

func get_input_scancode(event):
	if _check_key_pressed(event):
		var scancode = event.get_scancode()
		return scancode

func get_input_readable_scancode(event):
	if _check_key_pressed(event):
		return OS.get_scancode_string(event.scancode)
		
func get_input_readable_scancode_with_modifer(event):
	if _check_key_pressed(event):
		return OS.get_scancode_string(event.get_scancode_with_modifiers())
		
func _check_key_pressed(event):
	if event is InputEventKey and event.pressed:
		return true
	
	return false
