class_name States

var State = load("res://classes/State.gd")

var states = [
		State.new({
			"name" : "command",
			"description" : "Command mode",
			"command" : "!c",
			"focus" : "CommandLine"
		}),
		State.new({
			"name" : "help",
			"description" : "Help mode",
			"command" : "!h"
		}),
		State.new({
			"name" : "edit",
			"description" : "Edit mode",
			"command" : "!e",
			"focus" : "TextEditor"
		}),
	]

func _init():
	pass
