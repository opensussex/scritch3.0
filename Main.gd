extends Node2D

onready var TextEditor = get_node("TextEditor")
onready var Information = get_node("Information")
onready var CommandLine = get_node("CommandLine")
onready var Status = get_node("Status")

var States = load("res://classes/States.gd").new().states
var StateMachine = load("res://classes/StateMachine.gd").new(States)
var ScanCodes = load("res://classes/ScanCodes.gd").new()
var HotKeys = load("res://classes/HotKeys.gd").new({
	"Control+D" : "_hot_key_state_machine_cycle_state"
})

func _ready():
	ScanCodes.set_codes(HotKeys.keys)
	_state_has_changed()
	pass # Replace with function body.


func _state_has_changed():
	if(StateMachine.state["focus"]):
		var focusUIElement = StateMachine.state.focus
		get(focusUIElement).grab_focus()

#func _process(delta):
#	pass

func _input(event):
	var scancode = ScanCodes.get_input_readable_scancode_with_modifer(event)
	if (ScanCodes.has(scancode)):
		call(ScanCodes.get_method(scancode))


# Here are the methods that the hotkeys trigger
func _hot_key_state_machine_cycle_state():
	StateMachine.cycle_state()
	_state_has_changed()
